import os
import sys
from flask import Flask

app = Flask(__name__)
language = sys.argv[1].lower() if len(sys.argv) > 1 else 'english'
greeting = {
    'english': 'Hello',
    'german': 'Hallo',
    'french': 'Bonjour',
    'spanish': 'Hola',
    'italian': 'Ciao',
    'hindi': 'Namaste',
    'japanese': 'Konnichiwa',
    'turkish': 'Merhaba',
    'hungarian': 'Szia',
    'swahili': 'Jambo',
    'persian': 'Salaam'
}


@app.route('/')
def main():
    return '{} from '.format(greeting[language]) + os.getenv('HOSTNAME', "unknown") + "\n"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8080')
